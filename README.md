
# Expenses app server part

## Requirements

- express cli (installed globaly) `sudo npm install express-generator -g`
- mongodb database (installed globaly)
- eslint (installed globaly) `sudo npm i -g eslint`

## Things to start development

### after cloning

run comand below in your terminal
`$ npm i`

### start server

`$ DEBUG=expenses-app:* npm start`

### start mongodb

- `$ mongod --config /usr/local/etc/mongod.conf`
- `$ mongo` access to db via terminal (in new terminal tab/window)
