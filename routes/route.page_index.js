const express = require('express');
const router = express.Router();

/* GET index page */
router.get('/', (req, res, next) => {
  res.render('index', {
    title: 'Expense app',
    year: new Date().getFullYear(),
    routes: [
      'pricing',
      'about',
      'contacts'
    ]
  });
});

module.exports = router;
