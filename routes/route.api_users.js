const express = require('express');
const router = express.Router();

const controller_users = require('../controllers/controller.user');

/* GET users listing. */
router.get('/get-all-users', controller_users.getUsers);
router.get('/get-user-by-id/:id', controller_users.getUserById);
router.post('/create-user', controller_users.createUser);
router.put('/update-user/:id', controller_users.updateUser);
router.delete('/delete-user/:id', controller_users.deleteUser);

module.exports = router;
