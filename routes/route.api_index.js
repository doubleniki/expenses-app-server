const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  res.json({ info: 'Node.JS, Express & Postgres API'});
});

module.exports = router;
