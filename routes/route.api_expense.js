const express = require('express');
const router = express.Router();

const controller_expense = require('../controllers/controller.expense');

// a simple test url to check that all of our files are communicating correctly.
router.post('/create-espenses-table/:userId', controller_expense.createExpensesTable);
router.get('/get-expenses/:userId', controller_expense.getExpenses);
router.get('/get-expense-by-id/:userId/:id', controller_expense.getExpenseById);
router.put('/update-expense/:userId/:id', controller_expense.updateExpense);
router.delete('/delete-expense/:userId/:id', controller_expense.deleteExpense);
router.post('/create-expense/:userId/create', controller_expense.createExpense);

module.exports = router;
