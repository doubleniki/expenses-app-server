const pool = require('../db/db.config');

const createExpensesTable = (request, response) => {
  const { userId } = request.body.userId;
  const tableName = `expenses_${userId}`;
  const queryText =
    `CREATE TABLE ${tableName}
      (
        id UUID PRIMARY KEY,
        amount VARCHAR(128) NOT NULL,
        currency VARCHAR(3) NOT NULL,
        date DATE NOT NULL,
        walletId VARCHAR(128) NOT NULL,
        categoryId VARCHAR(128) NOT NULL
      )`;
  pool.query(queryText, (error, results) => {
    if (error) {
      throw  error
    }
    response.status(201).send(results)
  })
};

const getExpenses = (request, response) => {
  const userId = request.params.userId;
  const tableName = `expenses_${userId}`;
  const queryString = `SELECT * FROM ${tableName} ORDER BY date DESC`;
  
  pool.query(queryString, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
};

const getExpenseById = (request, response) => {
  const userId = request.params.userId;
  const id = request.params.id;
  const tableName = `expenses_${userId}`;
  const queryString = `SELECT * FROM ${tableName} WHERE id = ${id}`;
  
  pool.query(queryString, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows);
  })
};

const createExpense = (request, response) => {
  const {
    userId, amount, currency,
    date, walletId, categoryId
  } = request.body;
  const tableName = `expenses_${userId}`;
  const values = [amount, currency, date, walletId, categoryId];
  const queryString =
    `
    INSERT INTO ${tableName} (amount, currency, date, walletId, categoryId)
    VALUES ($1, $2, $3, $4, $5)
    `;
  
  pool.query(
    queryString, [...values], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).send(`Expense added with ID: ${results.insertId}`)
  })
};

const updateExpense = (request, response) => {
  const { userId, id, amount, currency, date, walletId, categoryId } = request.body;
  const tableName = `expenses_${userId}`;
  const queryString =
    `
    UPDATE ${tableName}
    SET amount = ${amount}, currency = ${currency}, date = ${date}, walletId = ${walletId}, categoryId = ${categoryId}
    WHERE id = ${id}
    `;
  
  pool.query(
    queryString, (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Expense with ID: ${id} has been modified`)
    }
  )
};

const deleteExpense = (request, response) => {
  const { userId, id } = request.body;
  const tableName = `expenses_${userId}`;
  const queryString =
    `DELETE FROM ${tableName} WHERE id = ${id}`;
  pool.query(queryString, (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`Expense with ID: ${id} has been deleted`)
  })
};

module.exports = {
  getExpenses,
  getExpenseById,
  createExpense,
  updateExpense,
  deleteExpense,
  createExpensesTable
};
