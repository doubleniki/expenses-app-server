require('dotenv').config();
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const helmet = require('helmet');
const cors = require('cors');
const handlebars = require('express-handlebars');
// const bodyParser = require('body-parser');
// const mongoose = require('mongoose');


// Pages routes
const indexPageRouter = require('./routes/route.page_index.js');

// API routes
const indexApiRouter = require('./routes/route.api_index.js');
const usersApiRouter = require('./routes/route.api_users.js');
const expenseApiRouter = require('./routes/route.api_expense.js');


const app = express();
app.engine('.hbs', handlebars({
  layoutsDir: path.resolve(__dirname, 'views', 'layouts'),
  partialsDir: path.resolve(__dirname, 'views', 'partials'),
  defaultLayout:'main',
  extname: '.hbs'
}));
app.set('view engine', '.hbs');

// adding Helmet to enhance your API's security
app.use(helmet());
// enabling CORS for all requests
app.use(cors());
// adding morgan to log HTTP requests
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// enabling compiling css from scss files
app.use(sassMiddleware({
  src: path.resolve(__dirname, 'styles'),
  dest: path.join(__dirname, 'public', 'styles'),
  prefix: '/styles',
  indentedSyntax: false, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexPageRouter);
// add api routes
app.use('/expenses_app/api', indexApiRouter);
app.use('/expenses_app/api/users', usersApiRouter);
app.use('/expenses_app/api/expenses', expenseApiRouter);


// app.use(express.static(path.join(__dirname, 'public')));

module.exports = app;
